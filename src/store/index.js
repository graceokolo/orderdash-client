import Vue from 'vue'
import Vuex from 'vuex'
import product from '../product/store'
import order from '../order/store'
import user from '../user/store'
import summaries from '../summaries/store'
import customer from '../customer/store'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  key: 'vuex',
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    drawer: null,
    baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
    baseResourceURL: process.env.VUE_APP_ORDERDASH_BASE_RESOURCE_URL
  },
  getters: {
    drawer: (state) => {
      return state.drawer
    }
  },
  plugins: [vuexLocal.plugin],
  mutations: {
  },
  actions: {
    changeDrawer: (context) => {
      context.commit('changeDrawer')
    }
  },
  modules: {
    product,
    order,
    user,
    customer,
    summaries
  }
})
