import axios from 'axios'
import Router from '@/router'

const state = {
  baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
  users: [],
  user: {},
  jwt: '',
  jwtRefresh: ''
}

const getters = {
  users: (state) => {
    return state.users
  },
  user: (state) => {
    return state.user
  },
  authDepartment: (state) => {
    return state.user.department.name
  },
  jwt: (state) => {
    return { 'Authorization': 'Bearer ' + state.jwt }
  },
  jwtRefresh: (state) => {
    return state.jwtRefresh
  }
}

const mutations = {
  setUsers: (state, payload) => {
    state.users = payload
  },
  setUser: (state, payload) => {
    state.user = payload
    localStorage.setItem('user', payload)
  },
  setJWT: (state, payload) => {
    state.jwt = payload
  },
  setJWTRefresh: (state, payload) => {
    state.jwtRefresh = payload
  },
  deleteUser: (state) => {
    state.user = null
    localStorage.setItem('user', null)
  },
  deleteJWT: (state) => {
    state.jwt = null
  },
  deleteJWTRefresh: (state) => {
    state.jwtRefresh = null
  }
}

const actions = {
  login: (context, payload) => {
    return axios.post(`${context.state.baseURL}token/`, { username: payload.username, password: payload.password })
  },
  logout: (context) => {
    context.dispatch('unauthenticate')
    Router.push({ name: 'login' })
  },
  setUser: (context, payload) => {
    context.commit('setUser', payload)
  },
  setJWT: (context, payload) => {
    context.commit('setJWT', payload)
  },
  setJWTRefresh: (context, payload) => {
    context.commit('setJWTRefresh', payload)
  },
  unauthenticate: (context) => {
    context.commit('deleteUser')
    context.commit('deleteJWT')
    context.commit('deleteJWTRefresh')
  },
  getAuthenticatedUser: (context) => {
    var jwtDecode = require('jwt-decode')
    const token = context.state.jwt
    const decoded = jwtDecode(token)
    const userID = decoded.user_id
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}users/${userID}/`
    })
  },
  refreshToken: (context) => {
    const payload = {
      refresh: context.state.jwtRefresh
    }
    axios.post(`${context.state.baseURL}token/refresh/`, payload)
      .then((response) => {
        context.commit('setJWT', response.data.access)
      })
  },
  inspectToken: (context) => {
    var jwtDecode = require('jwt-decode')
    const token = context.state.jwt
    const tokenRefresh = context.state.jwtRefresh
    if (token) {
      const decoded = jwtDecode(token)
      const decodedRefresh = jwtDecode(tokenRefresh)
      const exp = decoded.exp
      const expRefresh = decodedRefresh.exp
      // IF it is expiring in 7 minutes (420 second) AND it is not reaching its lifespan (1 day)
      if (exp - (Date.now() / 1000) < 420 && expRefresh - (Date.now() / 1000) > 1800) {
        context.dispatch('refreshToken')
        return false
      } else if (exp - (Date.now() / 1000) < 60 && expRefresh - (Date.now() / 1000) < 1800) {
        // IF it is expiring in 60sec
        context.dispatch('unauthenticate')
        Router.push({ name: 'login' })
        return false
      }
      return true
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
