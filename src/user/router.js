import LoginView from './views/LoginView'
import AUserView from './views/AUserView'

export default [
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  //   {
  //     path: '/register',
  //     name: 'register',
  //     component: RegisterView
  //   },
  //   {
  //     path: '/users',
  //     name: 'users',
  //     component: AllUsers
  //   },
  {
    path: '/users/:id',
    name: 'user',
    component: AUserView,
    props: { default: true }
  }
]
