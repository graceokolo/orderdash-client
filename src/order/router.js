import AllOrdersView from './views/AllOrdersView'
import CreateOrderView from './views/CreateOrderView'
import EditAnOrderView from './views/EditAnOrderView'
import ViewAnOrderView from './views/ViewAnOrderView'

export default [
  {
    path: '/orders',
    name: 'orders',
    component: AllOrdersView
  },
  {
    path: '/orders/create',
    name: 'create order',
    component: CreateOrderView
  },
  {
    path: '/orders/:id/',
    name: 'viewOrder',
    component: ViewAnOrderView,
    props: { default: true }
  },
  {
    path: '/orders/:id/edit',
    name: 'editOrder',
    component: EditAnOrderView,
    props: { default: true }
  }
]
