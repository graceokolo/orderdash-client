import axios from 'axios'
import Router from '@/router'

const state = {
  baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
  baseResourcesURL: process.env.VUE_APP_ORDERDASH_BASE_RESOURCE_URL,
  activeOrders: {},
  loadedOrder: {},
  tables: [],
  orderStatus: [],
  allOrders: []
}

const getters = {
  activeOrders: (state) => {
    return state.activeOrders
  },
  loadedOrder: (state) => {
    return state.loadedOrder
  },
  tables: (state) => {
    return state.tables
  },
  orderStatus: (state) => {
    return state.orderStatus
  },
  allOrders: (state) => {
    return state.allOrders
  }
}

const mutations = {
  setTables: (state, payload) => {
    state.tables = payload
  },
  setOrders: (state, payload) => {
    state.activeOrders = payload
  },
  setAllOrders: (state, payload) => {
    state.allOrders = payload
  },
  setAnOrder: (state, payload) => {
    state.loadedOrder = payload
  },
  setOrderStatus: (state, payload) => {
    state.orderStatus = payload
  }
}

const actions = {
  getTables: (context) => {
    axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}tables/`
    }).then(response => {
      context.commit('setTables', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  getRecentOrders: (context) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}orders/recently-active/`
    }).then(response => {
      context.commit('setOrders', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  getOrders: (context) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}orders/`
    }).then(response => {
      context.commit('setAllOrders', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  getCustomersOrders: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}orders?customer__id=${payload.customerID}`
    })
  },
  getCustomersActiveOrders: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}orders/in-progress?customer=${payload.customerID}`
    })
  },
  getCustomerOrdersSummaries: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}summaries/customer-orders/${payload.customerID}`
    })
  },
  getAnOrder: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}orders/${payload}`
    })
  },
  archiveAnOrder: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}orders/${payload.id}/archive-order/`
    })
  },
  updateAnOrder: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}orders/${payload.id}/`
    })
  },
  changeOrderStatus: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}orders/${payload.id}/change-status/`
    })
  },
  createAnOrder: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}orders/`
    })
  },
  getOrderStatus: (context) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseResourcesURL}order-status/`
    })
  },
  chargeCustomer: (context, order) => {
    var payload = {
      charge_customer: true
    }
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}orders/${order.id}/`
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
