import SaleSummariesView from './views/SaleSummariesView'
import ProductSummariesView from './views/ProductSummariesView'

export default [
  {
    path: '/summaries/sales',
    name: 'saleSummaries',
    component: SaleSummariesView
  },
  {
    path: '/summaries/products',
    name: 'productSummaries',
    component: ProductSummariesView
  }
]
