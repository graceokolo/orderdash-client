import axios from 'axios'
import Router from '@/router'

const state = {
  baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
  baseResourcesURL: process.env.VUE_APP_ORDERDASH_BASE_RESOURCE_URL,
  filteredSales: [],
  filteredProducts: []
}

const getters = {
  filteredSales: (state) => {
    return state.filteredSales
  },
  filteredProducts: (state) => {
    return state.filteredProducts
  }
}

const mutations = {
  setFilteredSales: (state, payload) => {
    state.filteredSales = payload
  },
  setFilteredProducts: (state, payload) => {
    state.filteredProducts = payload
  }
}

const actions = {
  getFilteredSales: (context, payload) => {
    axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}summaries/orders?start_date=${payload.start_date}&end_date=${payload.end_date}`
    }).then(response => {
      var reformattedActiveOrders = response.data.map(order => {
        order.paid = order.is_paid
        order.status = order.status.value
        return order
      })
      context.commit('setFilteredSales', reformattedActiveOrders)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  getFilteredProducts: (context, payload) => {
    axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}summaries/products?start_date=${payload.start_date}&end_date=${payload.end_date}&category__name=${payload.category}`
    }).then(response => {
      context.commit('setFilteredProducts', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
