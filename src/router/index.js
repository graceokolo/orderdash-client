import Vue from 'vue'
import VueRouter from 'vue-router'
import productRoutes from '../product/router'
import orderRoutes from '../order/router'
import userRoutes from '../user/router'
import customersRoutes from '../customer/router'
import dashboardRoutes from '../dashboard/router'
import summariesRoutes from '../summaries/router'
import Store from '@/store'

Vue.use(VueRouter)

const baseRoutes = [
  // { path: '/', name: 'home', component: HomePageView },
  { path: '*', redirect: '/' }
]

let routes = orderRoutes.concat(productRoutes)
routes = routes.concat(userRoutes)
routes = routes.concat(customersRoutes)
routes = routes.concat(baseRoutes)
routes = routes.concat(dashboardRoutes)
routes = routes.concat(summariesRoutes)

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('user')
  const inspectToken = Store.dispatch('inspectToken')
  if (authRequired && !loggedIn && inspectToken) {
    return next('/login')
  }
  if (to.name === 'orders' && Store.getters.authDepartment === 'Administration') {
    next()
  } else if (to.name === 'orders' && Store.getters.authDepartment !== 'Administration') {
    return next('/')
  }
  next()
})

export default router
