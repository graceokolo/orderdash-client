var parameters = {
  orderStatus: {
    open: 'Open',
    processing: 'Processing',
    awaitingPayment: 'Awaiting Payment',
    ready: 'Ready',
    completed: 'Completed',
    served: 'Served',
    archived: 'Archived'
  },
  productCategories: {
    food: 'Food',
    drinks: 'Drinks'
  },
  countries: {
    NG: 'Nigeria'
  },
  customerTransactionTypes: {
    credit: 'CREDIT'
  }
}

export { parameters }
