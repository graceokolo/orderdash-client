import axios from 'axios'
import Router from '@/router'

const state = {
  baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
  baseResourcesURL: process.env.VUE_APP_ORDERDASH_BASE_RESOURCE_URL,
  activeCustomers: [],
  customerPaginationNumber: 1,
  customerPaginationNextPageUrl: '',
  customerPaginationPreviousPageUrl: '',
  loadedCustomer: {},
  nameTitles: []
}

const getters = {
  customerPaginationNextPageUrl: (state) => {
    return state.customerPaginationNextPageUrl
  },
  customerPaginationPreviousPageUrl: (state) => {
    return state.customerPaginationPreviousPageUrl
  },
  customerPaginationNumber: (state) => {
    return state.customerPaginationNumber
  },
  activeCustomers: (state) => {
    return state.activeCustomers
  },
  loadedCustomer: (state) => {
    return state.loadedCustomer
  },
  nameTitles: (state) => {
    return state.nameTitles.map(item => item.value)
  },
  nameTitlesWithID: (state) => {
    return state.nameTitles
  }
}

const mutations = {
  setCustomerPaginationNextPageUrl: (state, payload) => {
    state.customerPaginationNextPageUrl = payload
  },
  setCustomerPaginationPreviousPageUrl: (state, payload) => {
    state.customerPaginationPreviousPageUrl = payload
  },
  setCustomerPaginationNumber: (state, payload) => {
    state.customerPaginationNumber = payload
  },
  setCustomers: (state, payload) => {
    state.activeCustomers = payload
  },
  setNameTitles: (state, payload) => {
    state.nameTitles = payload
  }
}

const actions = {
  getReducedCustomers: (context) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/reduced`
    })
  },
  getCustomers: (context) => {
    axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/`
    }).then(response => {
      context.commit('setCustomers', response.data.results)
      context.commit('setCustomerPaginationNextPageUrl', response.data.next)
      context.commit('setCustomerPaginationPreviousPageUrl', response.data.previous)
      context.commit('setCustomerPaginationNumber', response.data.number_of_pages)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  getACustomer: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload}`
    })
  },
  getCustomerPhoneNumbers: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload}/phone-numbers`
    })
  },
  getCustomerAddresses: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload}/addresses`
    })
  },
  createANewCustomerAddress: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}customers/${payload.customerID}/addresses/`
    })
  },
  deleteACustomerAddress: (context, payload) => {
    return axios({
      method: 'DELETE',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload.customerID}/addresses/${payload.id}/`
    })
  },
  updateAnAddress: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}addresses/${payload.id}/`
    })
  },
  getCountryCodes: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseResourcesURL}country-codes/`
    })
  },
  getCountries: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseResourcesURL}countries/`
    })
  },
  getNameTitle: (context) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseResourcesURL}name-titles/`
    }).then(response => {
      context.commit('setNameTitles', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        Router.push({ name: 'login' })
      }
    })
  },
  updateACustomer: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}customers/${payload.id}/`
    })
  },
  createACustomerPhoneNumber: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}customers/${payload.customer.id}/phone-numbers/`
    })
  },
  deleteCustomerPhoneNumber: (context, payload) => {
    return axios({
      method: 'DELETE',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload.customer.id}/phone-numbers/${payload.id}/`
    })
  },
  getCustomerTransactions: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}customers/${payload}/transactions/`
    })
  },
  createACustomerTransaction: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload.data,
      url: `${context.state.baseURL}customers/${payload.customerID}/transactions/`
    })
  },
  createACustomer: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}customers/`
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
