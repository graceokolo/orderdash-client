import AllCustomersView from './views/AllCustomersView'
import ACustomerView from './views/ACustomerView'
import CreateCustomerView from './views/CreateCustomerView'

export default [
  {
    path: '/customers',
    name: 'customers',
    component: AllCustomersView
  },
  {
    path: '/customers/:id/',
    name: 'customer',
    component: ACustomerView
  },
  {
    path: '/customer/create',
    name: 'newCustomer',
    component: CreateCustomerView
  }
]
