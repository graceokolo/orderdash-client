// import axios from 'axios'
// import Router from '@/router'

// const state = {
//   baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
//   baseResourcesURL: process.env.VUE_APP_ORDERDASH_BASE_RESOURCE_URL,
//   activeOrders: {},
//   loadedOrder: {},
//   tables: [],
//   orderStatus: []
// }

// const getters = {
//   activeOrders: (state) => {
//     return state.activeOrders
//   },
//   loadedOrder: (state) => {
//     return state.loadedOrder
//   },
//   tables: (state) => {
//     return state.tables
//   },
//   orderStatus: (state) => {
//     return state.orderStatus
//   }
// }

// const mutations = {
//   setTables: (state, payload) => {
//     state.tables = payload
//   },
//   setOrders: (state, payload) => {
//     state.activeOrders = payload
//   },
//   setAnOrder: (state, payload) => {
//     state.loadedOrder = payload
//   },
//   setOrderStatus: (state, payload) => {
//     state.orderStatus = payload
//   }
// }

// const actions = {
//   getTables: (context) => {
//     axios({
//       method: 'GET',
//       headers: context.getters.jwt,
//       data: {},
//       url: `${context.state.baseURL}tables/`
//     }).then(response => {
//       context.commit('setTables', response.data)
//     }).catch(error => {
//       if (error.response.status === 401) {
//         Router.push({ name: 'login' })
//       }
//     })
//   },
//   getOrders: (context) => {
//     axios({
//       method: 'GET',
//       headers: context.getters.jwt,
//       data: {},
//       url: `${context.state.baseURL}orders/`
//     }).then(response => {
//       context.commit('setOrders', response.data)
//     }).catch(error => {
//       if (error.response.status === 401) {
//         Router.push({ name: 'login' })
//       }
//     })
//   },
//   getAnOrder: (context, payload) => {
//     return axios({
//       method: 'GET',
//       headers: context.getters.jwt,
//       data: {},
//       url: `${context.state.baseURL}orders/${payload}`
//     })
//   },
//   archiveAnOrder: (context, payload) => {
//     return axios({
//       method: 'PATCH',
//       headers: context.getters.jwt,
//       data: payload,
//       url: `${context.state.baseURL}orders/${payload.id}/archive-order/`
//     })
//   },
//   updateAnOrder: (context, payload) => {
//     return axios({
//       method: 'PATCH',
//       headers: context.getters.jwt,
//       data: payload,
//       url: `${context.state.baseURL}orders/${payload.id}/`
//     })
//   },
//   changeOrderStatus: (context, payload) => {
//     return axios({
//       method: 'PATCH',
//       headers: context.getters.jwt,
//       data: payload,
//       url: `${context.state.baseURL}orders/${payload.id}/change-status/`
//     })
//   },
//   createAnOrder: (context, payload) => {
//     return axios({
//       method: 'POST',
//       headers: context.getters.jwt,
//       data: payload,
//       url: `${context.state.baseURL}orders/`
//     })
//   },
//   getOrderStatus: (context) => {
//     return axios({
//       method: 'GET',
//       headers: context.getters.jwt,
//       data: {},
//       url: `${context.state.baseResourcesURL}order-status/`
//     }).then(response => {
//       context.commit('setOrderStatus', response.data)
//     }).catch(error => {
//       if (error.response.status === 401) {
//         Router.push({ name: 'login' })
//       }
//     })
//   }
// }

// export default {
//   state,
//   getters,
//   mutations,
//   actions
// }
