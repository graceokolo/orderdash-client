import DashboardView from './views/DashboardView'

export default [
  {
    path: '/',
    name: 'dashboard',
    component: DashboardView
  }
  // {
  //   path: '/orders/create',
  //   name: 'create order',
  //   component: CreateOrderView
  // },
  // {
  //   path: '/orders/:id/',
  //   name: 'edit order',
  //   component: EditAnOrderView
  // }
]
