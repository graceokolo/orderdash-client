import AllProductsView from '../product/views/AllProductsView.vue'
import CreateProductView from '../product/views/CreateProductView.vue'
import EditProductView from '../product/views/EditProductView.vue'

export default [
  {
    path: '/products',
    name: 'products',
    component: AllProductsView
  },
  {
    path: '/products/create',
    name: 'newProduct',
    component: CreateProductView,
    props: { default: true }
  },
  {
    path: '/products/:id/',
    name: 'editProduct',
    component: EditProductView,
    props: { default: true }
  }
]
