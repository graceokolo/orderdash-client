import axios from 'axios'

const state = {
  baseURL: process.env.VUE_APP_ORDERDASH_BASE_URL,
  activeProducts: [],
  loadedProduct: {},
  productCategories: [],
  productCategoriesWithID: [],
  productPaginationNumber: 1,
  productPaginationNextPageUrl: '',
  productPaginationPreviousPageUrl: ''
}

const getters = {
  productPaginationNextPageUrl: (state) => {
    return state.productPaginationNextPageUrl
  },
  productPaginationPreviousPageUrl: (state) => {
    return state.productPaginationPreviousPageUrl
  },
  productPaginationNumber: (state) => {
    return state.productPaginationNumber
  },
  activeProducts: (state) => {
    return state.activeProducts
  },
  loadedProduct: (state) => {
    return state.loadedProduct
  },
  productCategories: (state) => {
    return state.productCategories.map(item => item.name)
  },
  productCategoryObjects: (state) => {
    return state.productCategories
  }
}

const mutations = {
  setProducts: (state, payload) => {
    state.activeProducts = payload
  },
  setAProduct: (state, payload) => {
    state.loadedProduct = payload
  },
  setProductCategories: (state, payload) => {
    state.productCategories = payload
  },
  setProductPaginationNextPageUrl: (state, payload) => {
    state.productPaginationNextPageUrl = payload
  },
  setProductPaginationPreviousPageUrl: (state, payload) => {
    state.productPaginationPreviousPageUrl = payload
  },
  setProductPaginationNumber: (state, payload) => {
    state.productPaginationNumber = payload
  }
}

const actions = {
  setProductStates: (context, payload) => {
    context.commit('setProducts', payload.results)
    context.commit('setProductPaginationNextPageUrl', payload.next)
    context.commit('setProductPaginationPreviousPageUrl', payload.previous)
    context.commit('setProductPaginationNumber', payload.number_of_pages)
  },
  getProducts: (context, payload) => {
    var searchText = ((payload && payload.searchText) ? payload.searchText : '')
    var categoryName = ((payload && payload.categoryName) ? payload.categoryName : '')
    var url = ''
    if (payload && payload.pagination) {
      (payload.pagination === 'next') ? (url = context.state.productPaginationNextPageUrl) : (url = context.state.productPaginationPreviousPageUrl)
    } else if (payload && payload.pageNumber) {
      url = `${context.state.baseURL}products/?search=${searchText}&category__name=${categoryName}&page=${payload.pageNumber}`
    } else {
      url = `${context.state.baseURL}products/?search=${searchText}&category__name=${categoryName}`
    }
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: url
    })
  },
  getProductsForOrders: (context, payload) => {
    var searchText = ((payload && payload.searchText) ? payload.searchText : '')
    var categoryName = ((payload && payload.categoryName) ? payload.categoryName : '')
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}products/?search=${searchText}&category__name=${categoryName}&page_size=8`
    })
  },
  getAProduct: (context, payload) => {
    return axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}products/${payload}`
    })
  },
  deleteAProduct: (context, payload) => {
    payload['is_active'] = 'false'
    const formData = new FormData()
    formData.append('id', payload['id'])
    formData.append('is_active', 'false')
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: formData,
      url: `${context.state.baseURL}products/${payload['id']}/`
    })
  },
  updateAProduct: (context, payload) => {
    return axios({
      method: 'PATCH',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}products/${payload.get('id')}/`
    })
  },
  createAProduct: (context, payload) => {
    return axios({
      method: 'POST',
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}products/`
    })
  },
  getProductCategories: (context) => {
    axios({
      method: 'GET',
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}products/categories`
    }).then(response => {
      context.commit('setProductCategories', response.data)
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        this.$router.push({ name: 'login' })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
